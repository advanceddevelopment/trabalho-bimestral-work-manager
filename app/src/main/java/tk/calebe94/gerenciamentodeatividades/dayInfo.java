package tk.calebe94.gerenciamentodeatividades;

import java.io.Serializable;

/**
 * Created by calebe94 on 05/04/18.
 */

public class dayInfo implements Serializable{
    private  String dayName;
    private String dayBalance;
    private int hourBalance = 0, minuteBalance = 0;
    private int importance = 0;
    public dayInfo(String name){
        this.dayName = name;
    }

    public String getDayName(){
        return this.dayName;
    }

    public void setDayName(String name){
        this.dayName = name;
    }
}
