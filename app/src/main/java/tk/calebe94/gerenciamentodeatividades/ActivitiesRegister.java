package tk.calebe94.gerenciamentodeatividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by calebe94 on 01/04/18.
 */

public class ActivitiesRegister extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if(intent !=null){
            Bundle paramns = intent.getExtras();
            if(paramns != null){
                String msg = paramns.getString("trc");
                Log.i("Teste", msg);
            }
        }
        setContentView(R.layout.activities_register);
    }
}