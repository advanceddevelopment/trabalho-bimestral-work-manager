package tk.calebe94.gerenciamentodeatividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    // Day's Buttons
    private Button buttonMonday = null;
    private Button buttonTuesday = null;
    private Button buttonWednesday = null;
    private Button buttonThursday = null;
    private Button buttonFriday = null;

    private Button buttonTasksRegister = null;
    private Button buttonChangeTrek = null;
    private Button buttonMainReport = null;

    private dayInfo mondayInfo = null;
    private dayInfo tuesdayInfo = null;
    private dayInfo wednesdayInfo = null;
    private dayInfo thursdayInfo = null;
    private dayInfo fridayInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mondayInfo    = new dayInfo("monday"   );
        this.tuesdayInfo   = new dayInfo("tuesday"  );
        this.wednesdayInfo = new dayInfo("wednesday");
        this.thursdayInfo  = new dayInfo("thursday" );
        this.fridayInfo    = new dayInfo("friday"   );
    }

    public void buttonMondayClicked(View view) {
        Intent intent =  new Intent(this, ActivitiesRecord.class);
        Bundle paramns = new Bundle();
        paramns.putString("trc", "monday");
        intent.putExtras(paramns);
        intent.putExtra("monday", this.mondayInfo);
        startActivity(intent);
    }

    public void buttonTuesdayClicked(View view) {
        Intent intent =  new Intent(this, ActivitiesRecord.class);
        Bundle paramns = new Bundle();
        paramns.putString("trc", "tuesday");
        intent.putExtras(paramns);
        intent.putExtra("tuesday", this.tuesdayInfo);
        startActivity(intent);
    }

    public void buttonWednesdayClicked(View view) {
        Intent intent =  new Intent(this, ActivitiesRecord.class);
        Bundle paramns = new Bundle();
        paramns.putString("trc", "wednesday");
        intent.putExtras(paramns);
        intent.putExtra("wednesday", this.wednesdayInfo);
        startActivity(intent);
    }

    public void buttonThursdayClicked(View view) {
        Intent intent =  new Intent(this, ActivitiesRecord.class);
        Bundle paramns = new Bundle();
        paramns.putString("trc", "thursday");
        intent.putExtras(paramns);
        intent.putExtra("thursday", this.thursdayInfo);
        startActivity(intent);
    }

    public void buttonFridayClicked(View view) {
        Intent intent =  new Intent(this, ActivitiesRecord.class);
        Bundle paramns = new Bundle();
        paramns.putString("trc", "friday");
        intent.putExtras(paramns);
        intent.putExtra("friday", this.fridayInfo);
        startActivity(intent);
    }

    public void buttonTasksRegisterClicked(View view) {
        Intent intent =  new Intent(this, ActivitiesRegister.class);
        Bundle paramns = new Bundle();
        paramns.putString("trc", "CADASTRO");
        intent.putExtras(paramns);
        startActivity(intent);
    }

    public void buttonDailyTrekClicked(View view) {
        Intent intent =  new Intent(this, DailyTrek.class);
        Bundle paramns = new Bundle();
        paramns.putString("trc", "JORDANA");
        intent.putExtras(paramns);
        startActivity(intent);
    }

    public void buttonMainReportClicked(View view) {
        Intent intent =  new Intent(this, ActivitiesReport.class);
        Bundle paramns = new Bundle();
        paramns.putString("trc", "RELATÓRIO");
        intent.putExtras(paramns);
        startActivity(intent);

    }
}

