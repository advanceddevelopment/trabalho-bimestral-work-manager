package tk.calebe94.gerenciamentodeatividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by calebe94 on 01/04/18.
 */

public class ActivitiesRecord extends AppCompatActivity {
    private dayInfo day = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activities_record);
        Intent intent = getIntent();

        if(intent !=null){
            Bundle bundle = (Bundle) intent.getExtras();
            String getDay = bundle.getString("trc");

            if      (getDay.contains("monday"))     this.day = (dayInfo) intent.getExtras().getSerializable("monday");
            else if (getDay.contains("tuesday"))    this.day = (dayInfo) intent.getExtras().getSerializable("tuesday");
            else if (getDay.contains("wednesday"))  this.day = (dayInfo) intent.getExtras().getSerializable("wednesday");
            else if (getDay.contains("thursday"))   this.day = (dayInfo) intent.getExtras().getSerializable("thursday");
            else if (getDay.contains("friday"))     this.day = (dayInfo) intent.getExtras().getSerializable("friday");

            if(day != null){
                String msg = day.getDayName();
                Log.i("Teste", msg);

            }
        }
    }
}
